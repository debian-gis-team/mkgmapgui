package tim.mkgmapgui;

/**
 * Poller to check operation thread
 * and report results back to controller
 */
public class ThreadPoller implements Runnable
{
	private GuiController _controller = null;
	private Thread _thread = null;


	/**
	 * Constructor
	 * @param inController controller
	 * @param inThread thread to watch
	 */
	public ThreadPoller(GuiController inController, Thread inThread)
	{
		_controller = inController;
		_thread = inThread;
	}

	/**
	 * Run method
	 */
	public void run()
	{
		boolean running = true;
		while (running)
		{
			try {
				Thread.sleep(2000);
			}
			catch (InterruptedException e) {}
			running = (_thread.getState() != Thread.State.TERMINATED);
		}
		_controller.operationComplete();
	}
}
