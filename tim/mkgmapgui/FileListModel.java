package tim.mkgmapgui;

import java.io.File;
import java.util.ArrayList;

import javax.swing.AbstractListModel;

/**
 * Class to hold the list of IMG Files to combine
 */
public class FileListModel extends AbstractListModel
{
	/** ArrayList of files */
	private ArrayList<File> _files = new ArrayList<File>();

	/**
	 * Add the file to the list
	 * @param inFile file to add
	 */
	public void addFile(File inFile)
	{
		if (!contains(inFile)) {
			_files.add(inFile);
		}
		this.fireContentsChanged(this, 0, getSize()-1);
	}

	/**
	 * Remove the specified file from the list
	 * @param inIndex index to remove
	 */
	public void removeFile(int inIndex)
	{
		_files.remove(inIndex);
		this.fireContentsChanged(this, 0, getSize()-1);
	}

	/**
	 * @return number of entries in list
	 */
	public int getSize() {
		return _files.size();
	}

	/**
	 * @param inIndex index to get
	 * @return list element
	 */
	public Object getElementAt(int inIndex) {
		return _files.get(inIndex).getName();
	}

	/**
	 * Check if the specified File is already in the list
	 * @param inFile file to check
	 * @return true if it's already present
	 */
	public boolean contains(File inFile)
	{
		int numFiles = getSize();
		for (int i=0; i<numFiles; i++) {
			if (_files.get(i).equals(inFile)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return array of file paths
	 */
	public String[] getFiles()
	{
		int numFiles = getSize();
		String[] filenames = new String[numFiles];
		for (int i=0; i<numFiles; i++) {
			filenames[i] = _files.get(i).getAbsolutePath();
		}
		return filenames;
	}
}
