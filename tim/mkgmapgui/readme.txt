Mkgmapgui version 1.1
=====================

Mkgmapgui is a graphical frontend for calling the mkgmap tool.
Full details can be found at http://activityworkshop.net/software/mkgmapgui/

Mkgmapgui is copyright activityworkshop.net and distributed under the terms of the Gnu GPL version 2.
You may freely use the software, and may help others to freely use it too.  For further information
on your rights and how they are protected, see the included license.txt file.

Mkgmapgui comes without warranty and without guarantee - the authors cannot be held responsible for
losses incurred through use of the program, however caused.


Running
=======

To run Mkgmapgui from the jar file, call it from a command prompt or shell and give it the path to mkgmap:
   java -Xmx512M -jar mkgmapgui.jar downloads/mkgmap

The -Xmx512M parameter allocates 512 MB to the java runtime, which is probably necessary for the conversion
step of mkgmap (depending on your map size).  The last, optional parameter specifies the directory in which
the mkgmap jar file can be found.

You can make a shortcut, menu item, alias, desktop icon or other link to make running it easier.

Version history
===============

1     First version
1.1   Added entry field for map number (required for merging later), fixed existing file detection


Further information and updates
===============================

To obtain the source code (if it wasn't included in your jar file), or for further information,
please visit the website:  http://activityworkshop.net/
