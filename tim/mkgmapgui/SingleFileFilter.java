package tim.mkgmapgui;

import java.io.File;
import javax.swing.filechooser.FileFilter;


/**
 * Class to act as a simple file filter based on a single file extension
 */
public class SingleFileFilter extends FileFilter
{
	/** Filter description for display */
	private String _filterDesc = null;
	/** Allowed file suffix */
	private String _suffix = null;

	/**
	 * Constructor
	 * @param inDescription filter description
	 * @param inSuffix allowed file suffix
	 */
	public SingleFileFilter(String inDescription, String inSuffix)
	{
		_filterDesc = inDescription;
		_suffix = inSuffix;
	}

	/**
	 * Check whether to accept the specified file or not
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 */
	public boolean accept(File inFile) {
		return inFile.isDirectory() || acceptFilename(inFile.getName());
	}

	/**
	 * Check whether to accept the given filename
	 * @param inName name of file
	 * @return true if accepted, false otherwise
	 */
	public boolean acceptFilename(String inName)
	{
		if (inName != null)
		{
			int nameLen = inName.length();
			if (_suffix != null && nameLen > _suffix.length())
			{
				return inName.toLowerCase().endsWith(_suffix);
			}
		}
		// Not matched so don't accept
		return false;
	}

	/**
	 * Get description
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	public String getDescription() {
		return _filterDesc;
	}

}
