package tim.mkgmapgui;

import java.io.File;

/**
 * Class to represent an operation of mkgmap
 * including input parameters and what to do with the output
 */
public class Operation
{
	private String[] _arguments = null;
	private File _outputFile = null;
	private File _requestedFile = null;

	/**
	 * Constructor
	 * @param inArgs command line arguments to mkgmap
	 * @param inOutputFile output file generated by mkgmap
	 * @param inRequestedFile requested output file
	 */
	public Operation(String[] inArgs, File inOutputFile, File inRequestedFile)
	{
		_arguments = inArgs;
		_outputFile = inOutputFile;
		_requestedFile = inRequestedFile;
		if (!_requestedFile.getName().toLowerCase().endsWith(".img")) {
			_requestedFile = new File(_requestedFile.getAbsolutePath() + ".img");
		}
	}

	/**
	 * Constructor for a convert operation
	 * @param inOsmFile osm file to convert
	 * @param inName name of map
	 * @param inRequestedFile file requested by user
	 * @return Operation object
	 */
	public static Operation makeConvertOperation(File inOsmFile, String inName, File inRequestedFile)
	{
		String[] args = new String[] {"--mapname=" + inName, inOsmFile.getAbsolutePath()};
		return new Operation(args, new File(inName + ".img"), inRequestedFile);
	}

	/**
	 * Constructor for a combine operation
	 * @param inFiles array of files to combine
	 * @param inRequestedFile file requested by user
	 * @return Operation object
	 */
	public static Operation makeCombineOperation(String[] inPaths, File inRequestedFile)
	{
		String[] args = new String[inPaths.length + 1];
		args[0] = "--gmapsupp";
		System.arraycopy(inPaths, 0, args, 1, inPaths.length);
		return new Operation(args, new File("gmapsupp.img"), inRequestedFile);
	}

	/**
	 * @return command line arguments for mkgmap
	 */
	public String[] getArgs()
	{
		return _arguments;
	}

	/**
	 * @return output file from mkgmap
	 */
	public File getOutputFile()
	{
		return _outputFile;
	}

	/**
	 * @return requested file
	 */
	public File getRequestedFile()
	{
		return _requestedFile;
	}
}
