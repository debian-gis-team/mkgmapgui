package tim.mkgmapgui;

/**
 * Interface for communicating back to the GUI
 */
public interface GuiController
{
	/**
	 * Inform the controller that the
	 * operation was successfully completed
	 */
	public void operationComplete();
}
