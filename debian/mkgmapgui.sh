#!/bin/sh
# Simple wrapper script used to start mkgmapgui in Debian

set -e

JAVA_CMDS="${JAVA_HOME}/bin/java"
JAVA_CMDS="${JAVA_CMDS} /usr/bin/java"

if [ -z "${JAVA_OPTS}" ] || [ "$(echo "${JAVA_OPTS}" | grep -c '\-Xmx')" -eq 0 ]; then
    JAVA_OPTS="${JAVA_OPTS} -Xmx512M"
fi

if [ -z "${JAVACMD}" ]; then
    for jcmd in ${JAVA_CMDS}; do
       if [ -x "$jcmd" ] && [ -z "${JAVACMD}" ]; then
          JAVACMD="$jcmd"
          break
       fi
    done
fi

if [ "${JAVACMD}" ]; then
   echo "Using ${JAVACMD} to execute mkgmapgui."
   exec "${JAVACMD}" $JAVA_OPTS -jar /usr/share/mkgmapgui/mkgmapgui.jar /usr/share/mkgmap/mkgmap.jar
else
   echo "No valid JVM found to run mkgmapgui."
   exit 1
fi
